﻿///$tab Settings
SET ThousandSep=',';
SET DecimalSep='.';
SET MoneyThousandSep=',';
SET MoneyDecimalSep='.';
SET MoneyFormat='$#,##0.00;($#,##0.00)';
SET TimeFormat='h:mm:ss TT';
SET DateFormat='M/D/YYYY';
SET TimestampFormat='M/D/YYYY h:mm:ss[.fff] TT';
SET MonthNames='Jan;Feb;Mar;Apr;May;Jun;Jul;Aug;Sep;Oct;Nov;Dec';
SET DayNames='Mon;Tue;Wed;Thu;Fri;Sat;Sun';
///$tab Data Load
[RawData]:
LOAD * INLINE [
    Date, Customer, Item, Amount
    1/1/2014, Acme Corp, Earthquake Pills, 90
    2/1/2014, Acme Corp, Earthquake Pills, 90
    3/1/2014, Acme Corp, Earthquake Pills, 90
    4/1/2014, Acme Corp, Earthquake Pills, 90
    5/1/2014, Acme Corp, Earthquake Pills, 90
    6/1/2014, Acme Corp, Earthquake Pills, 90
    7/1/2014, Acme Corp, Earthquake Pills, 90
    8/1/2014, Acme Corp, Earthquake Pills, 90
    9/1/2014, Acme Corp, Earthquake Pills, 90
    10/1/2014, Acme Corp, Earthquake Pills, 90
    11/1/2014, Acme Corp, Earthquake Pills, 90
    12/1/2014, Acme Corp, Earthquake Pills, 60
    1/1/2014, Acme Corp, TNT, 60
    2/1/2014, Acme Corp, TNT, 60
    3/1/2014, Acme Corp, TNT, 10
    4/1/2014, Acme Corp, TNT, 10
    5/1/2014, Acme Corp, TNT, 10
    6/1/2014, Acme Corp, TNT, 10
    7/1/2014, Acme Corp, TNT, 10
    8/1/2014, Acme Corp, TNT, 10
    9/1/2014, Acme Corp, TNT, 90
    10/1/2014, Acme Corp, TNT, 90
    11/1/2014, Acme Corp, TNT, 90
    12/1/2014, Acme Corp, TNT, 90
    1/1/2014, Acme Corp, Anvil, 90
    2/1/2014, Acme Corp, Anvil, 20
    3/1/2014, Acme Corp, Anvil, 20
    4/1/2014, Acme Corp, Anvil, 20
    5/1/2014, Acme Corp, Anvil, 20
    6/1/2014, Acme Corp, Anvil, 20
    7/1/2014, Acme Corp, Anvil, 20
    8/1/2014, Acme Corp, Anvil, 20
    9/1/2014, Acme Corp, Anvil, 20
    10/1/2014, Acme Corp, Anvil, 20
    11/1/2014, Acme Corp, Anvil, 20
    12/1/2014, Acme Corp, Anvil, 0
    1/1/2014, MomCorp, eyePhone, 0
    2/1/2014, MomCorp, eyePhone, 0
    3/1/2014, MomCorp, eyePhone, 0
    4/1/2014, MomCorp, eyePhone, 0
    5/1/2014, MomCorp, eyePhone, 0
    6/1/2014, MomCorp, eyePhone, 0
    7/1/2014, MomCorp, eyePhone, 0
    8/1/2014, MomCorp, eyePhone, 10
    9/1/2014, MomCorp, eyePhone, 10
    10/1/2014, MomCorp, eyePhone, 100
    11/1/2014, MomCorp, eyePhone, 100
    12/1/2014, MomCorp, eyePhone, 100
    1/1/2014, MomCorp, Bending Unit, 100
    2/1/2014, MomCorp, Bending Unit, 100
    3/1/2014, MomCorp, Bending Unit, 40
    4/1/2014, MomCorp, Bending Unit, 40
    5/1/2014, MomCorp, Bending Unit, 40
    6/1/2014, MomCorp, Bending Unit, 90
    7/1/2014, MomCorp, Bending Unit, 10
    8/1/2014, MomCorp, Bending Unit, 10
    9/1/2014, MomCorp, Bending Unit, 10
    10/1/2014, MomCorp, Bending Unit, 10
    11/1/2014, MomCorp, Bending Unit, 10
    12/1/2014, MomCorp, Bending Unit, 10
];

///$tab Calculate Previous Value
[Data]:
LOAD
	Date
	, Customer
	, Item
	, Amount
	, If(Customer = Previous(Customer) and Item = Previous(Item), Previous(Amount), 0) as [Prior Months Amount]
Resident
	[RawData]
Order By
	Customer
	, Item
	, Date;
	
DROP Table [RawData];